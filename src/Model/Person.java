package Model;

public class Person {
	private int x;
	private int y;
	private int speed;
	private int[] direction = new int[2];
	private boolean infected;
	
	public Person(int x, int y, int speed, boolean infected, int[] direction) {
		
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.infected = infected;
		this.direction = direction;
	}
	public int[] getDirection() {
		return direction;
	}
	public void setDirection(int[] direction) {
		this.direction = direction;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public boolean isInfected() {
		return infected;
	}
	public void setInfected(boolean infected) {
		this.infected = infected;
	}
	public void move() {
		this.x = this.x+(this.speed*direction[0]);
		this.y = this.y+(this.speed*direction[1]);	
	}
}
