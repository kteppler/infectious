package Model;

import java.util.Random;

public class Area {
	private int height;
	private int width;
	private int population;
	private int infections;
	
	private Person[] pop;
	
	public Area(int height, int width, int population,int infections) {
		this.height = height;
		this.width = width;
		this.population = population;
		this.pop = new Person[population];
		this.infections = infections;
		this.start();
	}
	
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getPopulation() {
		return population;
	}
	public void setPopulation(int population) {
		this.population = population;
	}
	public Person[] getPersons() {
		return pop;
	}
	
	
	public int getInfections() {
		return infections;
	}

	public void setInfections(int infections) {
		this.infections = infections;
	}

	public void start() {
		Random r = new Random();
		for (int i = 0; i < this.pop.length; i++) {
			boolean inf = false;
			if(i<this.infections) {
				inf=true;
			}
			int y =  r.nextInt(height);
			int x =  r.nextInt(width);
			int speed = 1;//r.nextInt(3);
			int factor1 = r.nextInt(2)==0?-1:1;//zufllig Richtung
			int factor2 = r.nextInt(2)==0?-1:1;//zufllige Richtung
			int move1 = r.nextInt(2);
			int move2 = r.nextInt(2);
			int[] direction = {move1*factor1,move2*factor2};
			this.pop[i] = new Person(x,y,speed,inf,direction);
		}
	}
	
	public void move() {
		for (int i = 0; i < this.pop.length; i++) {
			int[] direction = pop[i].getDirection();
			//System.out.println("Vor: "+direction[0]+" "+direction[1]);
			if (pop[i].getY() > height-1 || pop[i].getY() < 0+1) {
				direction[1] = direction[1] * (-1);
			}
			if (pop[i].getX() >= width-1 || pop[i].getX() < 0+1) {
				direction[0] = direction[0] * (-1);
			}
			//System.out.println("Nach:"+direction[0]+" "+direction[1]);
			infect();
			pop[i].setDirection(direction);
			pop[i].move();
			
		}
		
	
	}
	
	 public void infect() {
		 for (int i = 0; i < this.pop.length; i++) {
			 if(pop[i].isInfected()) {
				 for (int j = 0; j < pop.length; j++) {
					if(pop[i].getX()==pop[j].getX() && pop[i].getY()==pop[j].getY()&&pop[j].isInfected()==false) {
						pop[j].setInfected(true);
						infections++;
					}
				}
			 }
		 }
	 }

	
	
}
