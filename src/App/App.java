package App;
import Model.*;

public class App {

	public static void main(String[] args) {
		
		int people = 60; 
		int height = 150;
		int width = 150;
		int startingInfections = 5;
		int round = 1500;//Runden
		Area area = new Area(height,width,people,startingInfections);
		System.out.println("Infizierte");
		int[] chart = new int[round];
		for (int i = 0; i < chart.length; i++) {
			int y = area.getInfections();
			chart[i] = y;
			area.move();
		}
		
		//Grafische Ausgabe
		int aggregation = 15;//Zusammenfassen von Runden
		for (int i = people; i > 0; i--) {
			System.out.print(i);
			for (int j = 0; j < chart.length; j=j+aggregation) {
				if(chart[j]>=i) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		
		
		
		
		
		
		System.out.println("Anzahl Runden");
		
		
		System.out.println("Anzahl infizierter: "+area.getInfections());
	}

}
